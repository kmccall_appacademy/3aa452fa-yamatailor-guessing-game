# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  num_to_guess = rand(1..100)
  guesses = 1
  p 'Guess a number:'
  guess = gets.chomp.to_i
  until guess == num_to_guess
    if guess > num_to_guess && guess != num_to_guess
      p "Your guess of #{guess} was too high."
      p 'Guess a number:'
      guess = gets.chomp.to_i
    else
      p "Your guess of #{guess} was too low."
      p 'Guess a number:'
      guess = gets.chomp.to_i
    end
    if guess.to_i == 0
      raise NoMoreInput
    end
    guesses += 1
  end
  if guess == num_to_guess
    p guess
    p guesses
  end
end

if __FILE__ == $PROGRAM_NAME
  guessing_game
end
